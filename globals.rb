module Globals
  BACKGROUND_Z, BULLET_Z, PLAYER_Z, ASTEROID_Z, DASHBOARD_Z, TEXT_Z = *(0..5)
  # Z order deprecated here. See module ZOrder below.
  WIDTH, HEIGHT = 1024, 768
end

module ZOrder
  BACKGROUND,
    MENU_BACKGROUND,

    BULLET,
    PLAYER,
    ASTEROID,
    COMET,

    DASHBOARD,
    
    BUTTON,
    TEXT,
    MOUSE = *(0..9)
end

module Asteroids
  OID_MODES = [{
    :normal => ["../3d_renders/asteroid_md.png", "bang2_remix.ogg"],  #normal
    :large => ["../3d_renders/asteroid_lg.png", "bang1_remix.ogg"],
    :small => ["../3d_renders/asteroid_sm.png", "bang2_remix.ogg"],
    :mini => ["../3d_renders/asteroid_mn.png", "bang3_remix.ogg"]},
    {
    :normal =>["../3d_renders/billiard_green.png", "bang2_remix.ogg"],#billiard
    :large => ["../3d_renders/billiard_blue.png", "bang1_remix.ogg"],
    :small => ["../3d_renders/billiard_red.png", "bang2_remix.ogg"],
    :mini => ["../3d_renders/billiard_yellow.png", "bang3_remix.ogg"]},
    {
    :normal => ["../3d_renders/bubble_blue.png", "bubble_pop_remix.ogg"],  #bubble
    :large => ["../3d_renders/bubble_purple.png", "bubble_pop_remix.ogg"],
    :small => ["../3d_renders/bubble_green.png", "bubble_pop_remix.ogg"],
    :mini => ["../3d_renders/bubble_orange.png", "bubble_pop_remix.ogg"]},
    {
    :normal =>["../3d_renders/face_yellow.png", "face_explode3_remix.ogg"],#face
    :large => ["../3d_renders/face_green.png", "face_explode1_remix.ogg"],
    :small => ["../3d_renders/face_red.png", "face_explode2_remix.ogg"],
    :mini => ["../3d_renders/face_blue.png", "face_explode4_remix.ogg"]},
    {
    :normal =>["asteroid.png", "bang2.ogg"], #retro oids
    :large => ["asteroid.png", "bang1.ogg"],
    :small => ["asteroid.png", "bang2.ogg"],
    :mini => ["asteroid.png", "bang3.ogg"]},
    {
    :normal =>["billiard_green.png", "bang2.ogg"], #retro billiards
    :large => ["billiard_blue.png", "bang1.ogg"],
    :small => ["billiard_red.png", "bang2.ogg"],
    :mini => ["billiard_yellow.png", "bang3.ogg"]},
    {
    :normal =>["bubble.png", "bubble_pop.ogg"], #retro bubbles
    :large => ["bubble.png", "bubble_pop.ogg"],
    :small => ["bubble.png", "bubble_pop.ogg"],
    :mini => ["bubble.png", "bubble_pop.ogg"]}
  ]

end
