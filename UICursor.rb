
# 3 guesses as to what this does. For menus and the Level Constructor

class UICursor
  attr_reader :x, :y

  def initialize(window, screen)
    @window = window
    @screen = screen
    @sprite = window.load_tiles('pointer.png', 50, 50) # default image
    @sprite_i = 0.0
    @sprite_max = @sprite.size
    @x = window.mouse_x
    @y = window.mouse_y
  end

  def update
    @x = @window.mouse_x
    @y = @window.mouse_y
  end

  def draw
    @sprite_i += 0.2 # using a float so that we can increment smaller vals
    @sprite_i %= @sprite_max
    # draw mouse image centered on target -- easy to tell where blocks go
    @sprite[@sprite_i.floor.to_i].draw_rot(@window.mouse_x, @window.mouse_y, 
                                           ZOrder::MOUSE, 0, 0.5, 0.5)
  end

  def set_img(img, width, height, index = 0)
    @sprite = window.load_tiles(img, width, height)
  end

end
