include Gosu
include Globals

class Bullet < GameObject

  def initialize(screen, game, image, x, y, angle, color)
    super(screen)
    shot_speed = 10
    @sprite = image
    @x, @y = x, y
    @angle = angle
    @color = color
    @vel_x = offset_x(@angle, shot_speed)
    @vel_y = offset_y(@angle, shot_speed) 
    @length = 0
    @dead = false
  end

  def update
    @x += @vel_x
    @y += @vel_y
    @x %= WIDTH
    @y %= HEIGHT
    @length += 1
    vanish if @length > 60
    return !@dead
    true
  end

  def draw
    @sprite[@length % @sprite.size].draw_rot(@x, @y, BULLET_Z, @angle)
  end

  def vanish
    @dead = true
  end

  def obj_id
    return @color
  end
end
