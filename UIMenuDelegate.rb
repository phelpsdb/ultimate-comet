
class UIMenuDelegate
  attr_reader :item_spacing, :section_width, :margin

  def initialize(window, screen)
    @window = window # the Gosu::Window instance
    @screen = screen # the game screen hosting this menu
    @item_spacing = 50
    @section_width = 200
    @margin = 20
    @pos_x, @pos_y = 0, 0
    @section_number = 1
    @tallest_section = 0 # index of the tallest section used for height calc
    @height = 0
    @menu = UIMenu.new(window, self)
    @screen.push_menu(self)
  end

  def update
    @menu.update
  end

  def draw
    @menu.draw
  end

  def button_down(id)
    if id == Gosu::Button::MsLeft
      @menu.clicked(:left)
    elsif id == Gosu::Button::MsRight
      @menu.clicked(:right)
    elsif id == Gosu::Button::KbEscape
      menu_will_disappear # kill me
    end
  end

  def get_x_position
    @pos_x
  end

  def get_y_position
    @pos_y
  end

  def numberOfSections
    @section_number
  end

  def menuItemsInSection(section_index)
    #subclasses must implement this
  end

  def menuItemForIndex(row_index, section_index)
    #subclasses must implement this too
  end

  def menu_height
    return @menu.data_sections[@tallest_section].size * @item_spacing + 2*@margin
  end

  def menu_will_disappear
    @screen.pop_menu
    # override to include additional instruction before closing
  end

end
