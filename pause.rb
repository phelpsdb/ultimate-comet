include Gosu
include Globals

class Pause < Screen

  def initialize(game, game_screen)
    super(game)
    @options = ["Resume", "Quit"]
    @screen_font = Font.new(game, Gosu.default_font_name, 40)
    @select = 0
    @game_screen = game_screen
    @norm_color = 0xffffffff
    @select_color = 0xffffff00
    @pause_color = 0xff00ff00
  end

  def update
  end

  def draw
    @options.each_with_index do |option, i|
      color = (i == @select) ? @select_color : @norm_color
      @screen_font.draw_rel(option, WIDTH/2, (i+1)*100 + 200, 
        TEXT_Z, 0.5, 0.5, 1, 1, color)
    end
    @screen_font.draw_rel("PAUSED", WIDTH/2, 40, TEXT_Z, 0.5, 0.0, 1, 1, @pause_color)
  end

  def button_down(id)
    case id
    when Button::KbReturn
      do_command
    when Button::KbDown
      @select += 1 unless @select == 1
    when Button::KbUp
      @select -= 1 unless @select == 0
    end
  end

  def do_command
    command = @options[@select]
    case command
    when "Resume"
      @active = false
    when "Quit"
      @game_screen.deactivate
      @active = false
    end
  end

  def activate
    @active = true
  end

  def ended?
    !@active
  end

end
