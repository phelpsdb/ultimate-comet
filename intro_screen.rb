
# this is a special welcome screen in honor of gosu
# I made this animation completely by myself in Blender 3D
# So you'd better appreciate it, guys

include Globals

class IntroScreen < Screen

  def initialize(window)
    super(window)
    @background = window.images["../3d_renders/background_atmosphere.png"]
    @logo_sprite = window.load_tiles("gosu_logo_sprite.png", 320, 256)
    @sprite_i = 1 # not 0 for update reasons; we can skip frame 1
    @loops = 0
    @window = window
    @ready = false
  end

  def update
    @loops += 1
    @sprite_i = @loops/3
    if @sprite_i >= @logo_sprite.size
      #last frame of the logo sprite
      @ready = true
      @sprite_i = @logo_sprite.size - 1
    end
  end

  def draw
    return if !@active
    @background.draw(0, 0, ZOrder::BACKGROUND)
    @logo_sprite[@sprite_i.to_i].draw_rot(WIDTH/2, HEIGHT/2, ZOrder::TEXT,
                                          0, 0.5, 0.5, 2, 2)
    # doubling the factor
  end

  def button_down(id)
    if @ready
      kill
      FrontScreen.new(@window)
    end
  end

end
