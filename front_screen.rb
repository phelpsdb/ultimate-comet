
class FrontScreen < Screen
  attr_accessor :game_options
  attr_reader :music

  def initialize(window)
    super(window)
    @background = window.images["../3d_renders/front_background.png"]
    @window = window
    @open_mode = true # welcome screen, display no menu yet
    @game_options = Options.load_options
    @menu_stack = []
    @music = Gosu::Song.new(window, "soundfx/menu_music_stereo.ogg")
    @music.play(true) unless !@game_options["music"] # loop music 
    @music.volume = 0.3
  end

  def update
    if @menu_stack.empty?
      @open_mode = true # welcome screen, display no menu yet
      return
    end
    @menu_stack.last.update
  end

  def draw
    @background.draw(0, 0, ZOrder::BACKGROUND)
    @menu_stack.last.draw unless @menu_stack.empty?
  end

  def button_down(id)
    if @open_mode # if we're showing the welcome screen,
      if id == Gosu::Button::KbEscape
        kill
        return
      end
      @open_mode = false
      FrontMenu.new(@window, self)
    else 
      @menu_stack.last.button_down(id)
    end
  end

  def push_menu(menu)
    @menu_stack << menu
  end

  def pop_menu
    @menu_stack.pop
  end

end
