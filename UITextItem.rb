
class UITextItem < UIMenuItem
  
  def initialize(window, delegate, text)
    super(window, delegate, text)
    # action is called to delegate when the button is clicked.
    @box_image = window.images['../3d_renders/text_box.png'] # default image
    @t_color = 0xffaaff77
  end

  def update(x, y, width, height, mouse_x, mouse_y)
    # calculate dimensions of background image before drawing.
    # also determine whether the mouse is hovering over valid content.
    super # sets @x, @y, @width, @height, and @color
    border_x = width.to_f / 16
    border_y = height.to_f / 16
    @image_x, @image_y = @x + border_x, @y + border_y
    @image_w, @image_h = @width - 2*border_x, @height - 2*border_y
  end

  def draw
    super
    @box_image.draw_as_quad(@image_x, @image_y, @color,
                        @image_x + @image_w, @image_y, @color,
                        @image_x + @image_w, @image_y + @image_h, @color,
                        @image_x, @image_y + @image_h, @color,
                        ZOrder::BUTTON)
    @font.draw_rel(@text, @image_x + @image_w/2, @image_y + @image_h/2,
                   ZOrder::TEXT, 0.5, 0.5, 1, 1, @t_color)
  end

  def set_box_image(file_name)
    @box_image = @window.images[file_name]
  end

end
