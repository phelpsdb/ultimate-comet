
class EnterScoreMenu < UIMenuDelegate

  def initialize(window, screen, player_color, score, level)
    super(window, screen)
    @player_color = player_color
    @score = score
    @level = level
    @pos_x, @pos_y = 300, 200
    @section_width = 400
    @menu.build_menu
  end

  def button_down(id)
    if id == Button::KbEscape
      cancel_submission
    else
      super
    end
  end

  def menuItemsInSection(section_index)
    return 4 # instructions, name text field, OK button, Cancel button
  end

  def menuItemForIndex(row_index, section_index)
    case row_index
    when 0
      return UITextItem.new(@window, self, "Player #{@player_color}, Enter Your Name")
    when 1
      field = UITextField.new(@window, self, "")
      field.max_chars = 20
      return field
    when 2
      return UIButton.new(@window, self, "Submit", :submit_score)
    when 3
      return UIButton.new(@window, self, "Cancel", :cancel_submission)
    end
  end

  def submit_score(sender)
    Scores.submit_score(@menu.data_sections[0][1].text, @score, @level)
    @screen.pop_menu
    @screen.end_submitting_scores?
  end

  def cancel_submission(sender)
    @screen.pop_menu
    @screen.end_submitting_scores?
  end

end
