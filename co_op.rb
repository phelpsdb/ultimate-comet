include Globals
include Gosu

class Coop < GameScreen
  attr_reader :transitioning
  WARP_SPOTS = {:blue => [WIDTH/2 - 50, HEIGHT/2 - 50],
    :red => [WIDTH/2 + 50, HEIGHT/2 - 50],
    :green => [WIDTH/2 - 50, HEIGHT/2 + 50],
    :purple => [WIDTH/2 + 50, HEIGHT/2 + 50]
  }

  def initialize(window, options)
    super(window, options) 
    @players.each {|play| play.lives = 10}
    begin
      @music = Song.new(window, "soundfx/game_play_stereo.ogg")
    rescue
      @game_options["music"] = false
      puts "music failed to load"
    end
    @music.volume = @game_options["music_volume"] if @music
    @candidates = [] # high score candidates
    @backgrounds = ["nebula", "atmosphere", "alien", "satan", "far_out",
      "old_school", "old_sound"]
    @game_over = false
    @music.play(true) unless @game_options['music'] == false
    start_level
  end

  def co_op_update
    @players.reject! do |play| 
      if !play.obj_id
        @candidates << play
      end
      !play.obj_id
    end
    @level_text = "Level #{@level}"
    game_over if @players.empty?
    end_level if @asteroids.empty?
  end

  def player_score(player, obj_scored)
    case obj_scored
    when :asteroid
      player.add_score
    when :enemy
      player.add_score(3)
    end
  end

  def game_start
    (@level - 2).times do
      Task.new(self, @game, :wait => rand(6000) + 400) { Enemy.new(self, @game) }
    end
    (@level - 4).times do
      Task.new(self, @game, :wait => rand(6000) + 400) {BlackHole.new(self, @game)}
    end
    (@level - 6).times do |i|
      Task.new(self, @game, :wait => (i+1) * 1500) {Comet.new(self, @game)}
    end
    @hap_text = "Level #{@level}"
    Task.new(self, @game, :wait => 120) { @hap_text = "" }
    mode = 0
    mode = 1 if @level >= 5
    mode = 2 if @level >= 10
    mode = 3 if @level >= 15
    mode = 4 if @level >= 20
    mode = 5 if @level >= 25
    mode = 6 if @level >= 30
    @background = @game.images["../3d_renders/background_#{@backgrounds[mode]}.png"]
    ((@level + 2) / 2).times do
      Asteroid.new_asteroid(self, @game, mode, 0, 0, :large)
      Asteroid.new_asteroid(self, @game, mode, 0, 0, :normal)
    end
    if @game_options["sound"]
      @mocks[rand(@mocks.length)].play.volume = (@game_options["sound_volume"])**(0.5)
    end
      # The mocks are too quiet and too funny to ignore. square-root 
      #   the options value to obtain a higher but always legal value
#    life_pool = 0
#    @players.each do |player|
#      life_pool += player.lives
#      player.lives = 0
#      life_pool += 1 unless @level == 1
#    end
#    @players.each {|play| play.lives = life_pool / @players.length }
  end

  def end_level
    return if @transitioning
    @transitioning = true
    @hap_text = "Level Completed"
    @players.each {|play| play.level_end}
    Task.new(self, @game, :wait => 120) do
      @level += 1
      @hap_text = ""
      @transitioning = false
      start_level
    end
  end

  def warp_players
    @players.each do |play|
      play.angle = 0
      play.warp_to(WARP_SPOTS[play.obj_id][0], WARP_SPOTS[play.obj_id][1], false)
    end
  end

  def game_over
    return if @game_over
    @players.reject! do |play| 
    # just in case we are exiting from the pause menu
      play.lives = 1
      play.die(:quit)
      @candidates << play
      true
    end
    @hap_text = "GAME OVER"
    @game_over = true
    Task.new(self, @game, :wait => 300) { query_high_score }# high_scores_list }
    @mocks[1].play.volume = 1.0 # heh heh heh
  end

  def query_high_score
    @hap_text = "" # don't say "GAME OVER" anymore
    high_scores = []
    @candidates.each do |play|
      high_scores << play if Scores.is_highscore?(play.score * 100)
    end
    if high_scores.size > 0
      submit_scores(high_scores)
  #def initialize(window, screen, player_color, score, level)
    else
      display_high_scores_list
    end
  end

  def submit_scores(scorers)
    scorers.each do |player|
      EnterScoreMenu.new(@game, self, player.name, player.score * 100, @level)
    end
  end

  def end_submitting_scores?
    if @menu_stack.empty? # check if there are no more submissions to make
      display_high_scores_list
    end
  end

  def display_high_scores_list
    HighscoresMenu.new(@game, self)
=begin
    scores = Scores.load_current_scores
    text = "High Scores:\n"
    scores.each do |score|
      text += "#{score[:name]} Level #{score[:level]} \n#{score[:score]}\n" 
    end
    @score_text = Image.from_text(@game, text, default_font_name, 20, 10,
                                  400, :center)
=end
  end

  # called once the highscores_menu kills itself after a mouseclick
  def did_exit_score_display
    kill
  end

end
