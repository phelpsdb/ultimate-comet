include Gosu
include Globals

class Comet < GameObject

  def initialize(screen, game)
    super(screen)
    @screen = screen
    @game = game
    @images = {}
    @soundfx = {}
    icon = game.images["../3d_renders/comet.png"]
    particle = game.images["particle.png"]
    #outer_layer = game.images["comet_outer_layer.png"]
    @images["icon"] = icon
    @images["particle"] = particle
    #@images["layer"] = outer_layer
    appear = game.sounds["comet_appear_remix.ogg"]
    @soundfx["appear"] = appear
    @x, @y = (-50), (-50)
    @angle = @vel_angle = rand(25) + 122
    @particle_color = Color.new(255, 0, 0, rand(256))
    @outer_color = Color.new(0xffffffff)
    @outer_color.alpha = 100
    @particles = []
    @destroyed = false
    @soundfx["appear"].play if @screen.game_options["sound"]
  end

  def update
    @particles.reject! {|particle| !particle.update }
    @particles.each {|particle| particle.draw }
    @angle += 3
    @x += offset_x(@vel_angle, 3)
    @y += offset_y(@vel_angle, 3)
    #@images["layer"].draw_rot(@x, @y, DASHBOARD_Z, @vel_angle, 0.5, 0.3, 1, 1, @outer_color)
    5.times do 
      @particles << Particle.new(@screen, @game, @images["particle"], (@x + rand(81) - 40), (@y + rand(81) - 40), PLAYER_Z, :comet) 
    end
    if @x >= WIDTH + 500
      return false
      disappear
    end
    true
  end

  def draw
    @images["icon"].draw_rot(@x, @y, ASTEROID_Z, @angle, 0.5, 0.5)
  end

  def obj_id
    return :comet
  end

  def disappear
    @destroyed = true
  end

end
