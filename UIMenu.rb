include Gosu

# all game menus are < UIMenu

class UIMenu
  attr_reader :data_sections
  C = Color.new(0xffffffff) # background color for draw_as_quad; placeholder

  def initialize(window, delegate)
    @window = window
    @delegate = delegate
    @mouse = UICursor.new(window, self)
    @background = window.images["../3d_renders/menu.png"]
  end

  def build_menu
    @pos_x, @pos_y = @delegate.get_x_position, @delegate.get_y_position
    @spacing = @delegate.item_spacing
    @section_width = @delegate.section_width
    @margin = @delegate.margin

    #build the table section by section, item by item
    @data_sections = []
    @delegate.numberOfSections.times do |sec|
      # sections are columns, menu items are rows
      @data_sections << []
      @delegate.menuItemsInSection(sec).times do |i|
        @data_sections[sec] << @delegate.menuItemForIndex(i, sec)
      end
    end

    sections = @data_sections.size
    width = sections*@section_width + @margin*(sections + 1)
    height = @delegate.menu_height
    @dimensions = [width, height]
  end
  
  def update
    # always updating positions and dimensions for auto-resizing
    @mouse.update
    @pos_x, @pos_y = @delegate.get_x_position, @delegate.get_y_position
    @spacing = @delegate.item_spacing
    @section_width = @delegate.section_width
    @margin = @delegate.margin
    sections = @data_sections.size
    width = sections*@section_width + @margin*(sections + 1)
    height = @delegate.menu_height
    @dimensions = [width, height]

    # menu width = margin + sections*(margin+section_width) + margin
    # update the menu items by section
    @data_sections.each_with_index do |section, x|
      section.each_with_index do |item, y| 
        item.update(@pos_x + x*(@margin + @section_width) + @margin, 
                  @pos_y + @margin + @spacing*y,
                  @section_width, @spacing, @mouse.x, @mouse.y)
      end
    end
  end

  def draw
    @data_sections.each_with_index do |section, x|
      section.each_with_index do |item, y| 
        item.draw # will draw according to the position and dimensions
              # passed firsthand in update. This way, menu items can
              # calculate their internal content dimensions in update().
              # This should clear space for auto-resizing implementation
              # in future games implementing this UI system by allowing
              # items to dynamically set themselves up according to given
              # constraints.
      end
    end
    @background.draw_as_quad(@pos_x, @pos_y, C, 
                             @pos_x+@dimensions[0], @pos_y, C,
                             @pos_x+@dimensions[0], @pos_y+@dimensions[1], C,
                             @pos_x, @pos_y+@dimensions[1], C, 
                             ZOrder::MENU_BACKGROUND)
    @mouse.draw
  end

  # deprecated, give to @delegate or other higher power
  def button_down(id)
    if id == Button::MsLeft then clicked end
    if id == Button::KbEscape then kill end
  end

  def clicked(left_or_right)
    mx, my = @mouse.x, @mouse.y
    @data_sections.each do |section|
      section.each do |item|
        item.clicked(left_or_right, mx, my)
      end
    end
  end

  # deprecated, let the items handle this themselves
  def mouse_on_item?(item)
    mx, my = @window.mouse_x, @window.mouse_y
    x, y = item.x, item.y
    width, height = item.g[0..1]
    if mx >= x and my >= y and mx <= x + width and my <= y + height
      return true
    else
      return false
    end
  end

  def activate_button(name)
  end

end
