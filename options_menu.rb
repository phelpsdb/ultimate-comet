
class OptionsMenu < UIMenuDelegate
  OPTS_FILE = "options.yaml"

  def initialize(window, screen)
    super(window, screen)
    @section_number = 1
    @open_section = :none
    @section_width = 250
    @buttons = []
    button_names = ["Sound", "Players", "Controls", "Full Screen", 
      "Restore Defaults", "Reset High Scores", "Back"]
    button_actions = [:goto_sound, :goto_players, :goto_controls, 
      :goto_fullscreen, :goto_restore_defaults, :goto_scores, :goto_back]
    button_names.each_with_index do |btn, i|
      @buttons << UIButton.new(window, self, btn, button_actions[i])
    end
    @buttons.each {|btn| btn.set_button_image("../3d_renders/box_button.png")}
    reset_option_buttons
    @menu.build_menu
    @pos_x, @pos_y = 100, 100
  end

  def menuItemsInSection(section_index)
    case section_index
    when 0
      return @buttons.size
    when 1
      case @open_section
      when :sound
        return @sound_items.size  
      when :players
        return 1
      when :fullscreen
        return 4 # one for toggle, three for messages.
      end
    end
  end

  def menuItemForIndex(row_index, section_index)
    case section_index
    when 0
      return @buttons[row_index]
    when 1
      case @open_section
      when :sound
        return @sound_items[row_index]
      when :players
        return @player_item
      when :fullscreen
        case row_index
        when 0
          return @fullscreen_item if row_index == 0
        when 1
          return UITextItem.new(@window, self, "Requires Game Restart")
        when 2
          return UITextItem.new(@window, self, "For optimal performance,")
        when 3
          return UITextItem.new(@window, self, "do not use fullscreen.")
        end
      end
    end
  end

  def reset_option_buttons
    @sound_items = []
    @sound_items << UIToggleItem.new(@window, self, "Music",
                                    @screen.game_options["music"])
    @sound_items << UIToggleItem.new(@window, self, "Sound",
                                    @screen.game_options["sound"])
    @sound_items << UICounterItem.new(@window, self, "Sound Volume", [0,10])
    @sound_items << UICounterItem.new(@window, self, "Music Volume", [0,10])
    @sound_items[2].set_value(@screen.game_options["sound_volume"] * 10).to_i
    @sound_items[3].set_value(@screen.game_options["music_volume"] * 10).to_i

    @player_item = UICounterItem.new(@window, self, "Players", [1,4])
    @player_item.set_value(@screen.game_options["player_num"])
    
    @fullscreen_item = UIToggleItem.new(@window, self, "Fullscreen",
                                        @screen.game_options["fullscreen"])
  end

  def goto_sound(sender)
    if @open_section == :sound
      @open_section = :none
      @section_number = 1
      return
    end
    @section_number = 2
    @open_section = :sound
    @menu.build_menu
  end

  def goto_players(sender)
    if @open_section == :players
      @open_section = :none
      @section_number = 1
      return
    end
    @section_number = 2
    @open_section = :players
    @menu.build_menu
  end

  def goto_controls(sender)
    ControlsMenu.new(@window, @screen)
  end

  def goto_fullscreen(sender)
    if @open_section == :fullscreen
      @open_section = :none
      @section_number = 1
      return
    end
    @section_number = 2
    @open_section = :fullscreen
    @menu.build_menu
  end

  def goto_restore_defaults(sender)
    @screen.game_options = Options.load_default_options
    @window.game_controls = GameControls.load_default_inputs
    GameControls.save_inputs(@window.game_controls)
    Options.save_options(@screen.game_options)
    reset_option_buttons
    @menu.build_menu
  end

  def goto_scores(sender)
    scores = Scores.load_default_scores
    Scores.save_scores(scores)
  end

  def goto_back(sender)
    @screen.game_options["music"] = @sound_items[0].value
    if @sound_items[0].value == false
      @screen.music.stop if @screen.music.playing?
    else
      @screen.music.play unless @screen.music.playing?
    end
    @screen.game_options["sound"] = @sound_items[1].value
    @screen.game_options["player_num"] = @player_item.value
    @screen.game_options["fullscreen"] = @fullscreen_item.value
    @screen.game_options["sound_volume"] = @sound_items[2].value.to_f / 10.0
    @screen.game_options["music_volume"] = @sound_items[3].value.to_f / 10.0
    @screen.music.volume = @screen.game_options["music_volume"]
    Options.save_options(@screen.game_options)
    @screen.pop_menu
  end

  def menu_will_disappear
    goto_back(:kill)
    super
  end

end
