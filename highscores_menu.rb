
class HighscoresMenu < UIMenuDelegate

  def initialize(window, screen)
    super(window, screen)
    @pos_x = 20
    @pos_y = 50
    @margin = 18
    @item_spacing = 48
    @section_width = 220
    @section_number = 4
    @scores = Scores.load_current_scores
    @score_keys = [:name, :score, :level]
    @table_headers = ["Rank", "Name", "Score", "Level"]
    @menu.build_menu
  end

  def menuItemsInSection(section_index)
    return 11
  end

  def menuItemForIndex(row_index, section_index)
    if row_index == 0
      header = UITextItem.new(@window, self, @table_headers[section_index])
      header.t_color = 0xffff3388
      return header
      # return the column header name
    end
    if section_index == 0
      return UITextItem.new(@window, self, row_index.to_s)
      # return a simple rank index
    end
    return UITextItem.new(@window, self, @scores[row_index - 1][@score_keys[section_index - 1]])
    # return the corresponding score element's rank, name, score, or level
  end

  def button_down(id)
    if id == Button::MsLeft or id == Button::KbEscape
      @screen.pop_menu
      @screen.did_exit_score_display
    end
  end

end
