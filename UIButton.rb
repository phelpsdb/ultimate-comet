include Gosu

# These two classes operate in menus as either text or pics

class UIButton < UIMenuItem
  attr_reader :action

  def initialize(window, delegate, text, action)
    super(window, delegate, text)
    @action = action # symbol in the form of :method_name
    # action is called to delegate when the button is clicked.
    @button_image = window.images['../3d_renders/bar_button.png'] # default image
  end

  def update(x, y, width, height, mouse_x, mouse_y)
    # calculate dimensions of background image before drawing.
    # also determine whether the mouse is hovering over valid content.
    super # sets @x, @y, @width, @height, and @color
    border_x = width.to_f / 8
    border_y = height.to_f / 8
    @image_x, @image_y = @x + border_x, @y + border_y
    @image_w, @image_h = @width - 2*border_x, @height - 2*border_y
    return if @state == :disabled

    #ensure the button color stays normal unless mouse_over gets called.
    if mouse_x >= @image_x and mouse_x <= @image_x + @image_w and
      mouse_y >= @image_y and mouse_y <= @image_y + @image_h
        mouse_over
    else
      @state = :static
    end
  end

  def draw
    @background_image.draw_as_quad(@image_x, @image_y, @color,
                        @image_x + @image_w, @image_y, @color,
                        @image_x + @image_w, @image_y + @image_h, @color,
                        @image_x, @image_y + @image_h, @color,
                        ZOrder::BUTTON) if @background_image
    # background_image is optional, set if desired as a writable member.
    @button_image.draw_as_quad(@image_x, @image_y, @color,
                        @image_x + @image_w, @image_y, @color,
                        @image_x + @image_w, @image_y + @image_h, @color,
                        @image_x, @image_y + @image_h, @color,
                        ZOrder::BUTTON)
    @font.draw_rel(@text, @image_x + @image_w/2, @image_y + @image_h/2,
                   ZOrder::TEXT, 0.5, 0.5, 1, 1, @t_color)
  end

  def clicked(left_or_right, mx, my)
    return if @state != :hover
    case left_or_right
    when :left
      @selected_sound.play
      @delegate.method(@action).call(self)
    when :right
      # hmmm. Do nothing. But maybe add something later.
    end
  end

  def set_button_image(file_name)
    @button_image = @window.images[file_name]
  end

  def set_background_image(file_name)
    @background_image = @window.images[file_name]
  end

end

# DEPRECATED. UIImageButton features are now implemented by UIButton.
class UIImageButton
  attr_reader :name, :x, :y, :dimensions
  BORDER = 10

  def initialize(window, screen, x, y, image_name, name)
    @window, @screen, @x, @y, @name = window, screen, x, y, name
    @design = window.images[image_name]
    @image = window.images['button.png']
    @color = Color.new(0xffffffff)
    d_width, d_height = @design.width, @design.height
    i_width, i_height = @image.width, @image.height
    @dimensions = [d_width + 2*BORDER, d_height + 2*BORDER]
    @factor_x = (d_width + 2*BORDER).to_f / i_width.to_f
    @factor_y = (d_height + 2*BORDER).to_f / i_height.to_f
  end

  def draw
    @image.draw(@x, @y, ZOrder::BUTTON, @factor_x, @factor_y, @color)
    @design.draw(@x + BORDER, @y + BORDER, ZOrder::BUTTON)
    @color.green = @color.blue = 255
  end

  def mouse_over
    @color.green = @color.blue = 0
  end

end
