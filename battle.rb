include Gosu
include Globals

class Battle < GameScreen

  def initialize(window, options)
    super(window, options)
    @game_options = options
    case @game_options["mode"]
    when :survival
      @life_setting = 10
      @score_setting = 0
      background = "nebula"
    when :slaughter
      @life_setting = :infinite
      @score_setting = 0
      background = "satan"
    when :score
      @life_setting = :infinite
      @score_setting = 0
      background = "alien"
    when :slap_jack
      @life_setting = :infinite
      @score_setting = 5
      background = "atmosphere"
    end
    @background = window.images["../3d_renders/background_#{background}.png"]
    @players.each {|play| play.lives = @life_setting}
    @mode = @game_options["mode"]
    @round_winner = :none
    begin
      @music = Song.new(window, 'soundfx/battle_music_stereo.ogg')
    rescue
      @game_options["music"] = false
    end
    @music.volume = @game_options["music_volume"] if @music
    @music.play(true) unless @game_options['music'] == false
    start_level
  end

  def battle_update
    case @mode
    when :survival
      survival_update
    when :slaughter
      slaughter_update
    when :score
      score_update
    when :slap_jack
      survival_update
    end
    @level_text = "Round #{@level}"
  end

  def survival_update
    players_alive = []
    @players.each do |play|
      players_alive << play unless !play.obj_id
    end
    if players_alive.size == 1
      @round_winner = players_alive[0]
      end_level
    elsif players_alive.size == 0
      draw_game
    end
  end

  def slaughter_update
    @players.each do |play|
      if play.score >= 15
        @round_winner = play
        end_level
      end
    end
  end

  def score_update
    @players.each do |play|
      if play.score >= 20
        @round_winner = play
        end_level
      end
    end
  end

  def game_start
    @players.each do |play|
      play.score = @score_setting
      if !play.obj_id
        play.lives = @life_setting
        Speedometer.new(self, @game, play)
      end
      play.lives = @life_setting
    end
    10.times do |i|
      Task.new(self, @game, :wait => (i + 1)*1500) { Enemy.new(self,
                                                   @game) }
    end
    10.times do |i|
      Task.new(self, @game, :wait => (i + 1)*2000) { BlackHole.new(self,
                                                   @game) }
    end
    15.times do |i|
      Task.new(self, @game, :wait => i*1500) { Asteroid.new(self,
                                                  @game, 0, 0, 0, :large) }
    end
    3.times do |i|
      Task.new(self, @game, :wait => (i+1)*3500) { Comet.new(self, @game) }
    end
    @hap_text = "Round #{@level}"
    Task.new(self, @game, :wait => 120) {@hap_text = "" }
  end

  def end_level
    return if @transitioning
    @hap_text = "Round Over"
    @transitioning = true
    @players.each {|play| play.level_end}
    Task.new(self, @game, :wait => 200) do
      @level += 1
      @hap_text = ""
      @transitioning = false
      Results.new(@game, self, @players, @round_winner)
    end
  end

  def draw_game
    return if @hap_text == "DRAW"
    @hap_text = "DRAW"
    Task.new(self, @game, :wait => 200) do
      @hap_text = ""
      Results.new(@game, self, @players, :draw)
    end
  end

  def player_score(player, obj_scored)
    case @mode
    when :slaughter, :slap_jack
      return if obj_scored != :player
      player.add_score
    when :score
      return if obj_scored == :player
      score = (obj_scored == :asteroid) ? 1 : 3
      player.add_score(score)
    end
  end

  def warp_players
    @players.each do |play|
      play.angle = 0
      play.warp_to(rand(WIDTH - 200) + 100, rand(HEIGHT - 200) + 100, false)
    end
  end

  def game_over
    kill
  end

end
