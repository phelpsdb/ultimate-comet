include Gosu
include Globals

class FrontMenu < UIMenuDelegate
  START_X = WIDTH - 200
  START_Y = 100
  SPACING = 50
  TEXT_H = 20
  TEXT_COL = 0xff0055bb # greenish blue
  
  def initialize(window, screen)
    super(window, screen)
    @section_number = 1 # one column in this menu to start off
    @pos_x = 100
    @pos_y = 100

    @buttons = []
    button_names = ["Start Game", "Options", "Credits", "Exit"] 
    button_actions = [:goto_start, :goto_options, :goto_credits, :goto_exit]
    button_names.each_with_index do |button, i|
      @buttons << UIButton.new(window, self, button, button_actions[i])
    end
    
    # a second set of buttons to appear when user selects "start game"
    @game_buttons = []
    button_names = ["Adventure", "Survival", "Slaughter", "Score", 
                  "Slap Jack", "Star Fleet"]
    button_action = :start_game
    button_names.each do |button|
      @game_buttons << UIButton.new(window, self, button, button_action)
    end
    @game_buttons.each {|btn| btn.set_button_image("../3d_renders/box_button.png") }
    @menu.build_menu
  end

  def update
    super
    @game_buttons[1..4].each do |btn| 
      if @screen.game_options["player_num"] < 2
        btn.disable
      else
        btn.enable
      end
    end
    if @screen.game_options["player_num"] > 1
      @game_buttons[5].disable 
    else
      @game_buttons[5].enable
    end
  end

  def menuItemsInSection(section_index)
    case section_index
    when 0
      return @buttons.size
    when 1
      return @game_buttons.size
    end
  end

  def menuItemForIndex(row_index, section_index)
    case section_index
    when 0
      return @buttons[row_index]
    when 1
      return @game_buttons[row_index]
    end
  end

  def goto_start(sender)
    @section_number %= 2
    @section_number += 1
    # alternates either adding or removing another section to
    # display game buttons
    @tallest_section = @section_number - 1
    @menu.build_menu
  end

  def goto_options(sender)
    OptionsMenu.new(@window, @screen)
  end

  def goto_credits(sender)
    Credits.new(@window)
  end

  def goto_exit(sender)
    @screen.pop_menu
    @screen.kill
  end

  def start_game(sender)
    case sender.text
    when "Adventure" then mode = :co_op
    when "Survival" then mode = :survival
    when "Slaughter" then mode = :slaughter
    when "Score" then mode = :score
    when "Slap Jack" then mode = :slap_jack
    when "Star Fleet" then mode = :star_fleet
    end
    @screen.game_options["mode"] = mode
    @screen.music.stop if @screen.music.playing?
    if mode == :co_op
      Coop.new(@window, @screen.game_options)
    elsif mode == :star_fleet
      StarFleet.new(@window, @screen.game_options)
    else
      Battle.new(@window, @screen.game_options)
    end
  end

end
