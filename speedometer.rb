include Gosu
include Globals

class Speedometer
  def initialize(screen, game, player)
    screen.add_speedometer(self)
    @images = {}
    case player.obj_id
    when :blue
      @x, @y = 10, 15 # speed gauge position
      @t_x, @t_y = 150, 20 # score text position
      @i_x, @i_y = 115, 50 # life icon start position
      @dashboard = game.images["../3d_renders/dashboard.png"]
      @icon_color = 0xff0000ff
    when :red
      @x, @y = 585, 15
      @t_x, @t_y = 720, 20
      @i_x, @i_y = 688, 50
      @dashboard = game.images["../3d_renders/dashboard.png"]
      @icon_color = 0xffff0000
    when :green
      @x, @y = 10, 700
      @t_x, @t_y = 150, 723
      @i_x, @i_y = 120, 675
      @dashboard = game.images["../3d_renders/dashboard_B.png"]
      @icon_color = 0xff00ff00
    when :purple
      @x, @y = 585, 700
      @t_x, @t_y = 720, 723
      @i_x, @i_y = 688, 675 
      @dashboard = game.images["../3d_renders/dashboard_B.png"]
      @icon_color = 0xffff00ff
    end
    @images["dial"] = game.images["../3d_renders/dial.png"]
    @images["counter"] = game.images["../3d_renders/counter.png"]
    @images["player_icon"] = game.images["../3d_renders/space_ship.png"]
    @color = Color.new(0xffffffff)
    @speedometer_font = Font.new(game, Gosu.default_font_name, 30)
    @dial_angle = 0
    @screen = screen
    @game = game
    if screen.mode == :slaughter or screen.mode == :slap_jack
      @score_factor = 1
    else
      @score_factor = 100
    end
    @player = player
    @spacing = 25
    @factor = 0.5
  end

  def update
    angle = @player.current_speed
    @dial_angle = (15*angle) - 90
    @color.green -= 21 * angle.to_i
    @color.blue -= 21 * angle.to_i
    @text = "Score: #{@player.score * @score_factor}"
    unless @player.lives == :infinite
      @spacing = (@player.lives >= 14) ? 18 : 25
      @factor = (@player.lives >= 14) ? 0.4 : 0.5
    end
    #@screen.remove_speedometer(self) if @player.lives == 0
    @player.lives != 0 # does the same thing as above, now
  end
  
  def draw
    @images["counter"].draw(@x, @y, TEXT_Z, 1, 1, @color)
    @images["dial"].draw_rot(@x + 50, @y + 50, TEXT_Z, @dial_angle, 0.5, 0.5,
                             1, 1, @icon_color)
    case @player.obj_id # handle where to draw the dashboard
    when :blue
      @dashboard.draw(0, 0, DASHBOARD_Z, 1, 1, 0xff0000ff)
    when :red
      @dashboard.draw(573, 0, DASHBOARD_Z, 1, 1, 0xffff0000)
    when :green
      @dashboard.draw(0, (HEIGHT - 100), DASHBOARD_Z, 1, 1, 0xff00ff00)
    when :purple
      @dashboard.draw(573, (HEIGHT - 100), DASHBOARD_Z, 1, 1, 0xff880088)
    end
    @speedometer_font.draw(@text, @t_x, @t_y, TEXT_Z)
    unless @player.lives == :infinite
      @player.lives.times do |i|
        @images["player_icon"].draw(@i_x +(i * @spacing), @i_y, TEXT_Z, @factor, @factor, @icon_color)
      end
    end
    @color.blue = @color.green = 255
  end

end
