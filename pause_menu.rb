
class PauseMenu < UIMenuDelegate

  def initialize(window, screen)
    super(window, screen)
    @pos_x, @pos_y = 100, 100
    @menu.build_menu
  end

  def button_down(id)
    if id == KbEscape
      goto_continue(id)
    else
      super
    end
  end

  def menuItemsInSection(section_index)
    return 3 # continue, options, quit
  end

  def menuItemForIndex(row_index, section_index)
    case row_index
    when 0
      return UIButton.new(@window, self, "Continue", :goto_continue)
    when 1
      return UIButton.new(@window, self, "Options", :goto_options)
    when 2
      return UIButton.new(@window, self, "Quit", :goto_quit)
    end
  end

  def goto_continue(sender)
    @screen.paused = false
    @screen.pop_menu 
  end

  def goto_options(sender)
    OptionsMenu.new(@window, @screen)
  end

  def goto_quit(sender)
    @screen.paused = false
    @screen.pop_menu
    @screen.game_over
  end

end
