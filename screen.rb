
class Screen
  def initialize(game)
    game.add_screen(self)
    game.activate_screen
  end

  def update
  end

  def draw
  end

  def activate
    @active = true
  end

  def ended?
    !@active
  end

  def kill
    @active = false
  end

end
