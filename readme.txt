**************************************************
*           The Ultimate Comet Game              *
**************************************************

V2.0
Changes:

-Drastically improved graphics through Blender 3D
-Improved sound effects
-New smooth menu engine design influenced by my iPhone
  programming experience.
-New options include fullscreen ability, volume
  controls, and (clap your hands and shout hooray)
  CUSTOM CONTROL CONFIGURATION (this means you,
  game pad users)
-No more botched "re-deploy." Instead using respawn
  shields.
-ALL NEW EXCITING gameplay "Star Fleet." See
  for yourself.
-A whole heck of a lot more fun (wink wink)

THE SITE: code.google.com/p/ultimate-comet
ALSO SEE: www.libgosu.org/cgi-bin/mwf/topic_show.pl?tid=50

Created by phelps.db and rmphelps
But this is not the credits.
If you want credits, then read the credits. Duh.

--- GENERAL ----

Overview:
Ultimate Comet is a multiplayer space shooter game
created from Ruby and Gosu.

System Requirements:
This is not a "hard-core" game, let's be honest. 
If your computer can open this file, it can most
likely play this game.
The following MINIMUM "requirements" are more like
guidelines anyway.

General System Requirements:

512MB Memory
Intel Pentium 4

Additional requirements for source code:

Ruby 1.8+
Gosu (can be installed via gem install gosu)

That should keep it running relatively smooth.
Only really old computers or netbooks will
run into a mess.

Known Compatible Operating Systems:

Ubuntu 9.10 (Linux) - This is my system. Only
      has source code, however, because for some
      reason Mr. Gosu doesn't say how to package
      one on Linux.
Windows XP - Previously developed on here.
      Executable download available at THE SITE
Windows Vista - Why do you still have this?
      Executable download available at THE SITE
Windows 7 - The only sensible Windows since XP.
      Executable download available at THE SITE
OS X - App bundle will hopefully be provided 
      at THE SITE, but source works.

--- OPTIONS ---

The game defaults as fullscreen, but in the options
menu you can change that as well as sound and music
preferences.

Note: if you cannot access the game menus to change
the fullscreen setting, the correction can be made
in the file options.yaml by switching "true" to
"false" in line 8:

fullscreen: true

If options.yaml does not exist and you need to make
this change, create a regular text file named
"options.yaml" and enter the following lines:

---
player_num: 1
mode: :co_op
music: true
sound: true
sound_volume: 0.5
music_volume: 0.3
fullscreen: false

Hopefully, the ruby will read this as normal.

Note: The game window resolution is 1024x768. For
some operating systems, if the screen is too small
the window will not appear. To solve this, make
the appropriate change described above except with
fullscreen: true set. This is just in case you
cannot access this setting from the game menu.

Also Note:
FOR OPTIMAL PERFORMANCE avoid using fullscreen.
It is only applied by default for users with small
screens that don't fit a 1024x768 window.

--- GAMEPLAY ---

Adventure: (Single or Multiplayer)
Advance through increasingly difficult levels to
obtain a high score. Destroy all asteroids to
advance. The levels change as you
advance, so prepare for surprises.

Survival: (Multiplayer only)
Battle your friends. Last one alive wins the round.
10 lives distributed at the beginning.

Slaughter: (Multiplayer only)
Battle your friends in a Deathmatch. First player
to 20 kills wins the round.

Score: (Multiplayer only)
Compete with your friends to be the first to
achieve a score of 2000 to win the round.

Slap Jack: (Multiplayer only)
A combination of slaughter and survival. Stay
in the Game by killing other players to take
their points. Last one standing with points
wins the round.

Star Fleet: (Single player only)
All new adventure mode featuring a full fleet at 
your command. Keep at least one ship alive and 
destroy all asteroids to advance.
