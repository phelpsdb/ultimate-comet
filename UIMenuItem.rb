
class UIMenuItem
  attr_reader :x, :y, :width, :height, :state, :disabled
  attr_accessor :text, :t_color
  attr_accessor :hover_color, :static_color, :selected_color
  TEXT_H = 20

  def initialize(window, delegate, text)
    @window = window
    @delegate = delegate
    @text = text
    @t_color = 0xff000000 # default text color black
    @x, @y = 0, 0
    @width, @height = 0, 0
    @font = Font.new(@window, Gosu.default_font_name, TEXT_H)

    @hover_sound = window.sounds["scroll_remix.ogg"]
    @selected_sound = window.sounds["select_remix.ogg"]

    @disabled = false
    @state = :static

    @static_color = Color.new(0xffffffff) 
    # defualt color when the item is just sitting there bored. 
    @hover_color = Color.new(0xff9999ff) 
    # default color when the button is being hovered over by mouse.
    #@selected_color = Color.new(0xff0000ff)
    # default color when the button gets clicked
    @disabled_color = Color.new(0x99999999)
    # disabled color; user cannot select
    @color = @static_color
    # the current color
    
    @image_x, @image_y = 0, 0
    @image_w, @image_h = 0, 0
  end

  def update(x, y, width, height, mouse_x, mouse_y)
    # update dimensions and internal content based on given dimensions and
    # constraints for drawing. Allows for dynamic menu resizing.
    @x, @y = x, y
    @width, @height = width, height

    @state = (@disabled) ? :disabled : @state
    case @state
    when :static
      @color = @static_color
    when :hover
      @color = @hover_color
    when :disabled
      @color = @disabled_color
    end
  end

  def draw
    # draw the item within constraints received and recalculated in update.
    # Nothing in here right now. All up to the subclasses.
  end

  def clicked(left_or_right, ms_x, ms_y)
    # :left or :right. Usually only concerned with :left but includes
    # ability to act for :right clicks. Called by the UIMenu, this method
    # does not self-determine if a click has occurred.
  end

  def mouse_over
    if @state == :static
      @state = :hover 
      @hover_sound.play.volume = 0.1
    end
    # only enter the hover mode from static mode
  end

  def set_text_height(new_height)
    @font = Font.new(@window, Gosu.default_font_name, new_height)
  end

  def disable
    @disabled = true
  end

  def enable
    return if @disabled == false
    @disabled = false
    @state = :static
  end

end
