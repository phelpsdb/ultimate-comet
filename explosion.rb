require './particle'
require './gameobject'

class Explosion < GameObject
  def initialize(screen, game, image, explosion_img, x, y, z, mode=:normal)
    super(screen)
    @particles = []
    @e_img = explosion_img
    @e_factor = 0.05
    @e_fade_out = 3
    @e_color = Color.new(0xffffffff)
    @x, @y, @z = x, y, z
    case mode
    when :normal
      300.times {@particles << Particle.new(screen, game, image, x, y, z, mode)}
    when :asteroid
      100.times {@particles << Particle.new(screen, game, image, x, y, z, mode)}
    end
  end

  def update
    return false if @particles.empty? and @e_color.alpha < @e_fade_out
    @particles.reject! {|particle| !particle.update}
    @e_color.alpha -= @e_fade_out unless @e_color.alpha < @e_fade_out
    @e_factor += 0.05
    true
  end

  def draw
    @particles.each {|p| p.draw}
    @e_img.draw_rot(@x, @y, @z, 0, 0.5, 0.5, @e_factor, @e_factor, @e_color)
  end

  def obj_id
  end
end
