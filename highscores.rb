
class Scores

  HS_FILE = "highscores.yaml"

  def self.save_scores(scores)
    inputs_file = File.open(HS_FILE, "w") do |file|
      YAML.dump(scores, file)
    end
  end

  def self.load_current_scores
    if File.exists? HS_FILE
      return File.open(HS_FILE) {|file| YAML::load(file) }
    else
      return load_default_scores
    end
  end

  def self.load_default_scores
    scores = [
      {:name => "Master phelps.db", :score => "420100", :level => "46"},
      # yes, this is my true SICK score.
      {:name => "Dan", :score => "84200", :level => "20"},
      {:name => "Dumb Faces", :score => "48200", :level => "15"},
      {:name => "Beat This", :score => "36000", :level => "13"},
      {:name => "Aliens Got Me", :score => "31800", :level => "12"},
      {:name => "The Man", :score => "22500", :level => "10"},
      {:name => "The Man Too", :score => "22400", :level => "9"},
      {:name => "Fathead", :score => "8300", :level => "5"},
      {:name => "iSuck", :score => "4000", :level => "3"},
      {:name => "Foobar", :score => "100", :level => "1"}
    ]
    return scores
  end

  def self.is_highscore?(new_score)
    scores = load_current_scores
    if new_score > scores.last[:score].to_i
      true
    else 
      false
    end
  end

  def self.submit_score(new_name, new_score, new_level)
    scores = load_current_scores
    scores.pop
    scores.push({:name => new_name, 
                :score => new_score.to_s, 
                :level => new_level.to_s})
    scores = scores.sort {|x, y| y[:score].to_i <=> x[:score].to_i}
    save_scores(scores)
  end

end
