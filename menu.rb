include Gosu
include Globals

CO_OP = "Play an adventure game alone or teamed up with your friends. " +
  "No friendly fire."
SURVIVAL = "Each player has a set number of lives. Last one alive wins!"
SLAUGHTER = "The player that reaches a set number of kills first wins! " +
  "Infinite lives."
SCORE = "Destroy asteriods and aliens to reach a set number of points " +
  "first! Infinite lives."
SLAP_JACK = "Destroy your friends to steal their points. The player " +
  "who holds all the points wins!"

MODE_TEXTS = { "Co op Mode" => CO_OP, "Survival" => SURVIVAL,
  "Slaughter" => SLAUGHTER, "Score" => SCORE, "Slap Jack" => SLAP_JACK}

class Menu < Screen
  def initialize(game)
    super(game)
    @players = 1
    main_background = game.images["main_screen_bck.png"]
    main_options = ["Start Game", "Options", "Credits", "Exit"]
    player_background = game.images["player_screen_bck_new.png"]
    player_options = ["One Player", "Two Players", "Three Players", 
      "Four Players", "Keyboard Layouts", "Options"]
    game_mode_options = ["Co op Mode", "Survival", "Slaughter", "Score", 
      "Slap Jack", "Options"]
    game_mode_background = game.images["comet_bckgrnd.png"]
    options_options = ["Sound", "Players", "Set Play Mode", "Main"]
    options_background = game_mode_background
    layouts_background = game.images["layouts_screen_bck.png"]
    layouts_options = ["Options"]
    sound_background = game.images["sound_screen_bck.png"]
    sound_options = ["Sound On", "Sound Off", "Music On", "Music Off", 
      "Options"]
    credits_background = game.images["credits_screen_bck.png"]
    credits_options = ["Main"]
    main_screen = {
      "background" => main_background,
      "options" => main_options
    }
    player_screen = {
      "background" => player_background,
      "options" => player_options
    }
    options_screen = {
      "background" => options_background,
      "options" => options_options
    }
    layouts_screen = {
      "background" => layouts_background,
      "options" => layouts_options
    }
    game_mode_screen = {
      "background" => game_mode_background,
      "options" => game_mode_options
    }
    sound_screen = {
      "background" => sound_background,
      "options" => sound_options
    }
    credits_screen = {
      "background" => credits_background,
      "options" => credits_options
    }
    @areas = {"Main" => main_screen, "Players" => player_screen, 
      "Sound" => sound_screen, "Credits" => credits_screen,
      "Set Play Mode" => game_mode_screen, "Options" => options_screen,
      "Keyboard Layouts" => layouts_screen}
    @current_area = @areas["Main"]
    @game = game
    @screen_font = Font.new(game, Gosu.default_font_name, 50)
    @select = 0
    @select_color = Color.new(255, 255, 150, 0)
    @normal_color = Color.new(255, 255, 255, 255)
    @color = @normal_color
    @scroll_sound = game.sounds["scroll2.ogg"]
    @select_sound = game.sounds["select.ogg"]
    begin
      @background_music = Song.new(game, "soundfx/menu_music.ogg")
    rescue
      puts "Error: Music Failed to Load."
      puts "You may still need to obtain it and put it under"
      puts "the \"soundfx\" directory. Gameplay will continue"
      puts "without music."
    end
    @background_music.play if @background_music
    @options = {
      "sound" => true,
      "music" => true,
      "mode" => :co_op,
      "player_num" => 1
    }
  end

  def update
    begin
      @background_music.play unless @background_music.playing? or !@options["music"]
    rescue
      @options["music"] = false
    end
  end

  def draw
    @current_area["background"].draw(0, 0, 0)
    case @current_area
    when @areas["Players"]
      @screen_font.draw_rel("Players: #{@options["player_num"]}", 690, 20, 1, 1.0, 0.0) 
    when @areas["Set Play Mode"]
      @screen_font.draw(MODE_TEXTS[@current_area["options"][@select]], 10, HEIGHT - 50, TEXT_Z, 0.5, 0.5, 0xff009900)
    end
    @current_area["options"].each_with_index do |option, i|
      color = (@select == i) ? @select_color : @normal_color
      @screen_font.draw_rel(@current_area["options"][i], 974, ((i+1)*70 + 20), TEXT_Z, 1.0, 0.0, 1, 1, color)
    end
  end

  def button_down(id)
    case id
    when KbUp
      scroll(:up)
    when KbDown
      scroll(:down)
    when KbReturn
      do_command
    when KbEscape
      @active = false
    end
  end

  def activate
    @active = true
  end

  def ended?
    !@active
  end

  def scroll(direction)
    case direction
    when :up
      unless @select == 0
        @select -= 1
      else
        @select = @current_area["options"].length - 1
      end
    when :down
      @select += 1
      @select %= @current_area["options"].length
    end
    @scroll_sound.play if @options["sound"]
  end

  def do_command
    command = @current_area["options"][@select]
    @select_sound.play if @options["sound"]
    case command
    when "Start Game"
      @background_music.stop if @background_music
      if @options["mode"] == :co_op
        Coop.new(@game, @options)
      else
        Battle.new(@game, @options)
      end
    when "Options", "Players", "Sound", "Credits", "Main", "Set Play Mode", "Keyboard Layouts"
      @current_area = @areas[command]
      @select = 0
    when "Exit"
      @active = false
    when "One Player"
      @options["player_num"] = 1
    when "Two Players"
      @options["player_num"] = 2
    when "Three Players"
      @options["player_num"] = 3
    when "Four Players"
      @options["player_num"] = 4
    when "Co op Mode"
      @options["mode"] = :co_op
    when "Survival"
      @options["mode"] = :survival
    when "Slaughter"
      @options["mode"] = :slaughter
    when "Score"
      @options["mode"] = :score
    when "Slap Jack"
      @options["mode"] = :slap_jack
    when "Sound On"
      @options["sound"] = true
    when "Sound Off"
      @options["sound"] = false
    when "Music On"
      @options["music"] = true
      @background_music.play unless !@background_music or @background_music.playing?
    when "Music Off"
      @options["music"] = false
      @background_music.stop if @background_music
    end
  end

end
