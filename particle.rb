include Gosu

 class Particle
   def initialize(screen, game, image,  x, y, z, mode = :normal)
     @game = game
     @screen = screen
     @image = image
     @fadeout = 4
     @factor = (mode == :asteroid) ? 0.7 : 0.3
     @color = Color.new(255, 255, 69 + rand(186), 0)
     @x, @y, @z = x, y, z
     @velocity = rand * 3
     @image_angle = @angle = rand(360)
     comet_mode if mode == :comet
   end

   def update
     if @color.alpha <= @fadeout
       false
     else
       @color.alpha -= @fadeout
       @vy = offset_y(@angle, @velocity)
       @vx = offset_x(@angle, @velocity)
       @x += @vx
       @y += @vy
       @velocity -= 0.03 unless @velocity <= 0
       true
     end
   end

   def draw
     @image.draw_rot(@x, @y, @z, @image_angle, 0.5, 0.5, @factor, @factor, @color)
   end

   def comet_mode
     @fadeout = 3
     @velocity = 0
     @color = Color.new(255, 100, 100, rand(156) + 100)
   end
end
