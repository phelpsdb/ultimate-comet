
class Options
  OPTS_FILENAME = "options.yaml"

  def self.load_options
    if !File.exists? OPTS_FILENAME
      return load_default_options
    end
    return File.open(OPTS_FILENAME) {|file| YAML::load(file) }
  end

  def self.load_default_options
    return {"player_num" => 1, "mode" => :co_op, "music" => true,
      "sound" => true, "sound_volume" => 0.5, "music_volume" => 0.3,
      "fullscreen" => true } 
      # all the defaults
  end

  def self.save_options(options)
    File.open(OPTS_FILENAME, "w") do |file|
      YAML.dump(options, file)
    end
  end

end
