
class Credits < Screen

  def initialize(window)
    super(window)
    @window = window
    @background = window.images["../3d_renders/credits_screen_bck.png"]
  end

  def draw
    @background.draw(0, 0, ZOrder::BACKGROUND)
  end

  def button_down(id)
    kill
  end

end
