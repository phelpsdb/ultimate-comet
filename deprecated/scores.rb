require 'yaml'

# DEPRECATED, use highscores.rb Scores class instead.

class Scores

  def self.save_scores
    File.open("highscores.yaml", "w") do |file|
      file.puts "---"
      @@scores.each do |score, player|
        file.puts "#{score}: #{player}"
      end
    end
  end

  def self.load_current_scores
    @@scores = {}
    source = File.open "highscores.yaml"
    YAML::load_documents(source) do |scores|
      scores.each do |score, player|
        @@scores[score] = player
      end
    end
    return @@scores
  end

  def self.highscore?(new_score, new_player)
    load_current_scores
    high_scores = []
    @@scores.each do |score, player|
      high_scores << score
    end
    high_scores = high_scores.sort {|x, y| y <=> x}
    if new_score > high_scores.last
      old_score = high_scores.pop
      @@scores.delete(old_score)
      @@scores[new_score] = new_player
      save_scores
      true
    end
  end

end
