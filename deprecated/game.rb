# !/usr/bin/env ruby
require 'player'
require 'gameobject'
require 'bullet'
require 'powerup'
require 'explosion'
require 'particle'
require 'task'
require 'results'
require 'asteroid'
require 'special'
require 'speedometer'
require 'pause'
include Gosu
include Globals

class GameScreen < Screen
  attr_reader :player_num, :level, :players, :sound
  def initialize(game, player_num, co_op, sound, music)
    super(game)
    @objects = []
    @players = []
    @tasks = []
    @speedometers = []
    @bullet_to_player_id = {}
    @background = Image.new(game, "images/comet_bckgrnd.bmp", true)
    @dashboard_A = Image.new(game, "images/dashboard.bmp", true)
    @dashboard_B = Image.new(game, "images/dashboard_B.bmp", true)
    @player_num = player_num
    @player_blue = Player.new(self, game, :blue)
    @players << @player_blue
    @bullet_to_player_id["blue_bullet"] = @player_blue
    Speedometer.new(self, game, @player_blue)
    if player_num > 1
      @player_red = Player.new(self, game, :red)
      @players << @player_red
      @bullet_to_player_id["red_bullet"] = @player_red
      Speedometer.new(self, game, @player_red)
    end
    if player_num > 2
      @player_green = Player.new(self, game, :green)
      @players << @player_green
      @bullet_to_player_id["green_bullet"] = @player_green
      Speedometer.new(self, game, @player_green)
    end
    if player_num > 3
      @player_purple = Player.new(self, game, :purple)
      @players << @player_purple
      @bullet_to_player_id["purple_bullet"] = @player_purple
      Speedometer.new(self, game, @player_purple)
    end
    @mocks = []
    6.times {|i| @mocks << Sample.new(game, "soundfx/mock#{i+1}.wav")}
    @game = game
    @sound = sound
    @music = music
    @co_op = co_op
    @player_num = player_num
    @transitioning = false
    @level = 1
    @asteroid_image = Image.new(game, "images/asteroid.bmp", true)
    @display = Font.new(game, Gosu.default_font_name, 30)
    @hap_font = Font.new(game, Gosu.default_font_name, 60)
    music = 'soundfx/hovertheme1.mid' if @co_op
    music = 'soundfx/level3_final.mid' if !@co_op
    @background_music = Song.new(game, music)
    @background_music.volume = (0.5)
    start_level
  end

  def check_buttons
    if @game.button_down? Button::KbLeft
      @player_blue.turn_left
    end
    if @game.button_down? Button::KbRight
      @player_blue.turn_right
    end
    if @game.button_down? Button::KbUp
      @player_blue.accelerate
    end
    if @game.button_down? Button::KbDown
      @player_blue.reverse
    end
    if @game.button_down? Button::KbSpace
      @player_blue.shoot
    end
    if @game.button_down? Button::KbNumpad8
      @player_red.accelerate
    end
    if @game.button_down? Button::KbNumpad4
      @player_red.turn_left
    end
    if @game.button_down? Button::KbNumpad6
      @player_red.turn_right
    end
    if @game.button_down? Button::KbNumpad5
      @player_red.reverse
    end
    if @game.button_down? Button::KbEnter
      @player_red.shoot
    end
    if @game.button_down? @game.char_to_button_id("'")
      @player_green.turn_right
    end
    if @game.button_down? @game.char_to_button_id(";")
      @player_green.reverse
    end
    if @game.button_down? @game.char_to_button_id("l")
      @player_green.turn_left
    end
    if @game.button_down? @game.char_to_button_id("p")
      @player_green.accelerate
    end
    if @game.button_down? @game.char_to_button_id("\\")
      @player_green.shoot
    end
    if @game.button_down? @game.char_to_button_id("s")
      @player_purple.accelerate
    end
    if @game.button_down? @game.char_to_button_id("z")
      @player_purple.turn_left
    end
    if @game.button_down? @game.char_to_button_id("c")
      @player_purple.turn_right
    end
    if @game.button_down? @game.char_to_button_id("x")
      @player_purple.reverse
    end
    if @game.button_down? Button::KbLeftShift
      @player_purple.shoot
    end
  end
 
  def update
    check_buttons
    @objects.reject! { |obj| !obj.update }
    @tasks.reject! { |task| !task.tick }
    @powerups.reject! { |p| p.obtained }

# for battle mode, the last player standing wins.
# for co op mode, lifeless players are removed from the game
    if @co_op
      @players.reject! { |play| !play.obj_id }
    else
      players_alive = []
      @players.each do |play|
        players_alive << play unless !play.obj_id
      end
      if players_alive.size == 1
        @round_winner = players_alive[0]
        next_level
      end
    end
    @speedometers.each {|speed| speed.update}
    @b_text = @r_text = @g_text = @p_text = ""
    @level_text = "Level #{@level}" if @co_op
    @level_text = "Round #{@level}" if !@co_op
    @players.each do |player|
      if @asteroids.empty? and @co_op
        next_level
      elsif player.score >= 15 and !@co_op
        @round_winner = player
        next_level
      end
    end
    game_over if @players.empty?
    check_collision
  end
##### Check for collision-------------------------------------
  def check_collision
    @objects.each { |obj|
      id = obj.obj_id
      case id
      when "blue_bullet", "red_bullet", "green_bullet", "purple_bullet" 
        @players.each {|player| obj.vanish if player.check(obj.x, obj.y, 25) unless player == @bullet_to_player_id[id] or @co_op }
        @asteroids.each do |oid|
          @bullet_to_player_id[id].add_score and obj.vanish if oid.check(obj.x, obj.y, 27)
        end
        @specials.each do |spec|
          @bullet_to_player_id[id].add_score(3) and obj.vanish if spec.check(obj.x, obj.y, 30)
        end
      when :enemy_bullet
        @players.each { |player| player.check(obj.x, obj.y, 25) }
      when :blue, :red, :purple, :green 
        @players.each do |player|
          if distance(player.x, player.y, obj.x, obj.y) < 30 and player != obj
            unless obj.is_dead or player.is_dead
             player.die unless player.invincible?
             obj.die unless obj.invincible?
            end
          end
        end
      when "asteroid"
        @players.each {|player| player.check(obj.x, obj.y, 40 * obj.factor)}
      when "powerup"
        @players.each do |player|
        obj.obtain(player) if player.check_powerup(obj.x, obj.y, 30)
        end
      when :black_hole
        @players.each do |player|
          player.check(obj.x, obj.y, 10) 
          #player.vel_x *= 0.97
          #player.vel_y *= 0.97
        end
      when :enemy
        @players.each {|player| obj.die if player.check(obj.x, obj.y, 30)}
      when :comet
        @players.each {|player| player.check(obj.x, obj.y, 60) }
        @asteroids.each {|oid| oid.check(obj.x, obj.y, 70) }
        @specials.each {|spec| spec.check(obj.x, obj.y, 60) }
      end
    }
  end
  def button_down(id)
    # Exit the game with Esc key
    case id
    when Button::KbEscape
      deactivate
    when Button::KbReturn
      Pause.new(@game, self)
    end
  end

  def draw
    @background.draw(0, 0, BACKGROUND_Z)
    @dashboard_A.draw(0, 0, DASHBOARD_Z)
    @dashboard_B.draw(0, (HEIGHT - 71), DASHBOARD_Z) if @player_num > 2
    @objects.each { |obj| obj.draw }
    @speedometers.each {|speed| speed.draw}
    @display.draw_rel(@level_text, WIDTH/2, 20, TEXT_Z, 0.5, 0.5)
    @hap_font.draw_rel(@hap_text, WIDTH/2, HEIGHT/2, TEXT_Z, 0.5, 0.5)
  end

  def start_level
    @transitioning = false
    @objects = []
    @tasks = []
    @asteroids = []
    @powerups = []
    @specials = []
    co_op_start if @co_op
    battle_start if !@co_op
    @background_music.play unless !@music
    @players.each do |player|
      player.vel_x = player.vel_y = player.current_speed = 0
      player.normalize
      add_object(player)
      player.warp_to rand(WIDTH), rand(HEIGHT)
    end
  end

  def co_op_start
    (@level - 2).times do
      Task.new(self, @game, :wait => rand(6000) + 400) { Special.new(self, @game, :enemy) }
    end
    (@level - 4).times do
      Task.new(self, @game, :wait => rand(6000) + 400) {Special.new(self, @game, :black_hole)}
    end
    (@level - 6).times do |i|
      Task.new(self, @game, :wait => (i+1) * 1500) {Special.new(self, @game, :comet)}
    end
    @hap_text = "Level #{@level}"
    Task.new(self, @game, :wait => 120) { @hap_text = "" }
    mode = 0
    mode = 1 if @level >= 5
    mode = 2 if @level >= 10
    mode = 3 if @level >= 15
    ((@level + 2) / 2).times do
      Asteroid.new_asteroid(self, @game, mode, 0, 0, :large)
      Asteroid.new_asteroid(self, @game, mode, 0, 0, :normal)
    end
    @mocks[rand(@mocks.length)].play unless !@sound
#    life_pool = 0
#    @players.each do |player|
#      life_pool += player.lives
#      player.lives = 0
#      life_pool += 1 unless @level == 1
#    end
#    @players.each {|play| play.lives = life_pool / @players.length }
  end

  def battle_start
    @players.each do |play| 
      play.score = 0
      if play.lives == 0
        play.lives = 10
        @objects << play
        Speedometer.new(self, @game, play)
      end
      play.lives = 10
    end
    5.times do |i|
      Task.new(self, @game, :wait => (i + 1) * 1500) { Special.new(self, @game, :enemy) }
    end
    4.times do |i|
      Task.new(self, @game, :wait => (i + 1) * 2000) { Special.new(self, @game, :black_hole) }
    end
    10.times do |i|
      Task.new(self, @game, :wait => i * 800) { Asteroid.new(self, @game, 
                                                    0, 0, 0, :large) }
    end
    @hap_text = "Round #{@level}"
    Task.new(self, @game, :wait => 120) { @hap_text = "" }
    @lives = 10
  end

  def activate
    @active = true
  end

  def deactivate
    @background_music.stop
    @active = false
  end

  def ended?
    !@active
  end

  def next_level
    return if @transitioning
    @hap_text = "Level Completed" if @co_op
    @hap_text = "Round Over" if !@co_op
    @transitioning = true
    Task.new(self, @game, :wait => 120) do
      @level += 1
      @hap_text = ""
      @transitioning = false
      if !@co_op
        Results.new(@game, self, @players, @round_winner)
      else
        start_level
      end
    end
  end

  def game_over
    return if @hap_text == "GAME OVER"
    @hap_text = "GAME OVER"
    Task.new(self, @game, :wait => 300) { @active = false }
    @mocks[0].play
    @background_music.stop
  end

  def add_task(task)
    @tasks << task
  end

  def add_object(obj)
    @objects << obj
  end

  def add_asteroid(asteroid)
    @asteroids << asteroid
  end

  def remove_asteroid(asteroid)
    @asteroids -= [asteroid]
  end

  def add_powerup(powerup)
    @powerups << powerup
  end

  def add_speedometer(meter)
    @speedometers << meter
  end

  def remove_speedometer(meter)
    @speedometers -= [meter]
  end

  def add_special(special)
    @specials << special
  end
  
  def remove_special(special)
    @specials -= [special]
  end

end
