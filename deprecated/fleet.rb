
include Gosu
include Globals

# this class is meant to be treated exactly like a Player, except 
# it utilizes an arrangement of players that act as one.
class Fleet
  attr_accessor :x, :y, :vel_x, :vel_y, :current_speed, :angle
  attr_accessor :lives, :score, :wins, :deaths
  attr_reader :is_dead, :id, :name

  def initialize(window, screen, color)
    @window = window
    @screen = screen
    @color = color
  end

end
