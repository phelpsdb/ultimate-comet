require 'rubygems'
require 'gosu'
require './globals'
require './screen'
require './UICursor'
require './UIMenu'
require './UIMenuDelegate'
require './UIMenuItem'
require './UIButton'
require './UITextField'
require './UITextItem'
require './UIToggleItem'
require './UICounterItem'
require './intro_screen'
require './controls_menu'
require './credits_screen'
require './game_controls'
require './options'
require './options_menu'
require './pause_menu'
require './front_menu'
require './enter_score_menu'
require './highscores_menu'
require './front_screen'
require './new_game'
require './star_fleet'
require './battle'
require './co_op'
require './highscores'
include Gosu
include Globals

class GameWindow < Window
  attr_accessor :images, :sounds, :game_controls

  def initialize
    options = Options.load_options
    fullscreen = options["fullscreen"]
    super(WIDTH, HEIGHT, fullscreen)
    self.caption = "The Ultimate Comet Game"
    @screens = []
    @images = Hash.new {|hash, key| hash[key] = Image.new(self, "images/#{key}", true) }
    @sounds = Hash.new {|hash, key| hash[key] = Sample.new(self, "soundfx/#{key}") }
    @game_controls = GameControls.load_inputs
    IntroScreen.new(self)
  end

  def update
    @current_screen = @screens.last
    if @current_screen.ended?
      @screens.pop
      if @screens.length == 0
        close
      end
    end
    @current_screen.update
  end
  
  def draw
    @current_screen.draw
  end

  def button_down(id)
    @current_screen.button_down(id)
  end

  def add_screen(screen)
    @screens << screen
  end

  def activate_screen
    @current_screen = @screens.last
    @current_screen.activate
  end

  # new method added 6/18/10 for enhanced graphics
  def load_tiles(filename, width, height, tileable = true)
    if !@images.has_key?(filename)
      @images[filename] = Image.load_tiles(self, "3d_renders/#{filename}", 
                                           width, height, tileable)
    end
    return @images[filename]
  end

end

GameWindow.new.show
