include Gosu
include Globals

class Powerup < GameObject
  TYPES = [:speed, :slow, :turn, :turn_slow, :invincibility,
    :super_shot, :life, :score]

  def initialize(screen, game, x, y, type)
    super(screen)
    screen.add_powerup(self)
    @obtained = false
    case type
    when :speed
      powerup_image = "powerup_speed.png"
      img_w, img_h = 40, 45
      powerup_sound = "powerup_speed_remix.ogg"
    when :invincibility
      powerup_image = "powerup_invincibility.png"
      img_w, img_h = 40, 40
      powerup_sound = "powerup_invincibility_remix.ogg"
    when :slow
      powerup_image = "powerup_slow.png"
      img_w, img_h = 40, 45
      powerup_sound = "powerup_slow_remix.ogg"
    when :turn
      powerup_image = "powerup_turn.png"
      img_w, img_h = 45, 40
      powerup_sound = "powerup_turn_remix.ogg"
    when :turn_slow
      powerup_image = "powerup_turn_slow.png"
      img_w, img_h = 45, 40
      powerup_sound = "powerup_turn_slow_remix.ogg"
    when :super_shot
      powerup_image = "powerup_super_shot.png"
      img_w, img_h = 34, 34
      powerup_sound = "powerup_charge_remix.ogg"
    when :life
      powerup_image = "powerup_life.png"
      img_w, img_h = 50, 50
      powerup_sound = "powerup_life_remix.ogg"
      if screen.mode == :slaughter or screen.mode == :score or screen.mode == :slap_jack then
        @obtained = true
      end
    when :score
      powerup_image = "powerup_score.png"
      img_w, img_h = 50, 50
      powerup_sound = "powerup_score_remix.ogg"
      if screen.mode == :slaughter or screen.mode == :survival
        @obtained = true
      end
    end
    @sprite = game.load_tiles(powerup_image, img_w, img_h, true)
    @fp = 3 # frame progression; number of game cycles before sprite frame +
    @img_inc = 0.0 # image increment
    @obtain_sound = game.sounds[powerup_sound]
    @screen = screen
    @game = game
    @type = type
    @vel_x = @vel_y = rand(3) - 1
    @draw_angle = 0
    @travel_angle = rand(360)
    #wait_task = Task.new(game, :wait => rand(600)) {appear}
    @x, @y = x, y
    Task.new(@screen, @game, :wait => 1000) { @obtained = true }
    appear
  end

  def update
    return false if @obtained 
    @draw_angle += 1
    @img_inc += 1.0/@fp # image index remains a float, to_i later
    @img_inc %= 27
    @x += offset_x(@travel_angle, @vel_x)
    @y += offset_y(@travel_angle, @vel_y)
    @x %= WIDTH
    @y %= HEIGHT
    true
  end

  def draw
    @sprite[@img_inc.floor.to_i].draw_rot(@x, @y, 1, @draw_angle)
  end

  def self.random(screen, game, x, y)
    Powerup.new screen, game, x, y, TYPES[rand(TYPES.size)]
  end

  def appear
    @angle = rand(360)
  end

  def obj_id
    return "powerup"
  end

  def obtain(player)
    return if @obtained # error prevention if game style does not permit this
                        #     particular powerup
    @obtained = true
    if @screen.game_options["sound"]
      @obtain_sound.play.volume = (@screen.game_options["sound_volume"])**2
      # too loud, most of them. Square it to slightly decrement the volume
    end
    player.powerup(@type)
  end

  def obtained
    return @obtained
  end
end

