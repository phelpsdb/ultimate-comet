require './gameobject'
require './explosion'
require './task'
include Gosu
include Globals

class Asteroid < GameObject
  TYPES = [:normal, :large, :small, :mini]
attr_reader :factor
  def initialize(screen, game, image_mode_num, x, y, type)
    super(screen)
    screen.add_asteroid(self)
    @color = Color.new(0xffffffff)
    @color.alpha = 220 if image_mode_num == 2
    # image_mode_num explained:
    # 1 => regular asteroids
    # 2 => billiard asteroids
    # 3 => bubble asteroids
    # 4 => face asteroids
    @screen = screen
    @game = game
    @degenerated = false
    @id = "asteroid"
    @x, @y = x, y
    @type = type
    @image_mode_num = image_mode_num
    @image = @game.images[Asteroids::OID_MODES[image_mode_num][type][0]]
    @e_img = @game.images["asteroid_explosion.png"]
    case type
    when :rand
      type = TYPES[rand(2)]
    when :normal
      @factor = 1
    when :large
      @factor = 1.2
    when :small
      @factor = 0.8
    when :mini
      @factor = 0.5
    end
    @explode_sound = game.sounds[Asteroids::OID_MODES[image_mode_num][type][1]]
    particle_image = "asteroid_particle.png" if image_mode_num == 0
    particle_image = "billiard_particle.png" if image_mode_num == 1
    particle_image = "bubble_particle.png" if image_mode_num == 2
    particle_image = "asteroid_particle.png" if image_mode_num == 3
    particle_image = "asteroid_particle.png" if image_mode_num == 4
    particle_image = "billiard_particle.png" if image_mode_num == 5
    particle_image = "bubble_particle.png" if image_mode_num == 6
    @particle = @game.images["../3d_renders/#{particle_image}"]
    appear
  end

  def update
    @x += @vel_x
    @y += @vel_y
    @x %= WIDTH
    @y %= HEIGHT
    @image_angle += 1
    return false if @degenerated
    true
  end

  def draw
    @image.draw_rot(@x, @y, ASTEROID_Z, @image_angle, 0.5, 0.5, @factor, @factor, @color) unless @degenerated
  end

  def appear
    speed = rand(3) + 1
    vel_angle = rand(360)
    @image_angle = rand(360)
    @vel_x = offset_x(vel_angle, speed)
    @vel_y = offset_y(vel_angle, speed)
    unless @type == :small or :mini
      @x = rand(WIDTH)
      @y = rand(HEIGHT)
    end
  end

  def self.new_asteroid(screen, game, modeNum, x = 0, y = 0, type = :normal)
    Asteroid.new(screen, game, modeNum, x, y, type)
  end

  def obj_id
    return @id
  end

  def check(x, y, radius)
    if !@degenerated
      if distance(x, y, @x, @y) < (radius * @factor)
        degenerate
        return true
      else
        return false
      end
    end
  end

  def degenerate
    @degenerated = true
    if @image_mode_num != 2 or @type == :normal or @type == :mini
      Explosion.new(@screen, @game, @particle, @e_img, @x, @y, 1, :asteroid)
    end
    if @screen.game_options["sound"]
      @explode_sound.play.volume = @screen.game_options["sound_volume"]
    end
    @id = "destroyed obj"
    if @screen.game_options["mode"] != :star_fleet
      Powerup.random(@screen, @game, @x, @y) if rand(2) == 0
    end
    case @type
    when :large
      2.times {Asteroid.new_asteroid(@screen, @game, @image_mode_num, @x,@y, :small)}
    when :small
      Task.new(@screen, @game, :wait => 1) do
      2.times {Asteroid.new_asteroid(@screen, @game, @image_mode_num, @x, @y, :mini)}
      end
    end
    Task.new(@screen, @game, :wait => 10) {@screen.remove_asteroid(self)}
  end

end
